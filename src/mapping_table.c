#include "mapping_table.h"

static uint32_t get_next_block(uint32_t eraseblock) 
{
    while (badBlockTable[eraseblock] != GOODBLOCK) {
        ++eraseblock;
    }

    return eraseblock;
}

static uint32_t erase_ftl_block(uint32_t eraseblock) 
{
    while (_erase(eraseblock) < 0) {
        badBlockTable[eraseblock] = 0;
        ++eraseblock;
    }

    return eraseblock;
}

ftl_addr *logical_to_physical(uint32_t lpn, mapping_table *table) 
{
    /*
     * lpn: logical page number
     * mapping_table: pointer to the current mapping table
     * convert logical page number to block and page offset
     * returns: address of physical block and page offset
     */
    uint32_t logical_block = lpn / PAGES_PER_BLOCK;

    if (!table) { 
        return NULL; 
    }
    // requested block must exist in table
    if (logical_block >= table->size) { 
        return NULL; 
    }
    ftl_addr *addr = malloc(sizeof(ftl_addr));
    if (!addr) { 
        return NULL; 
    }
    addr->block = table->entries[logical_block]->phys_block;
    addr->page_offset = lpn % PAGES_PER_BLOCK;
    return addr;
}

mapping_table *init_mapping_table(uint32_t fd, uint32_t table_size) 
{
    /*
     * fd: file descriptor pointing to nand flash
     * returns: table of physical block numbers
     */
    mapping_table *table;
    uint32_t size = ENTRIES_PER_PAGE;
    uint32_t pages = 0;
    uint32_t ppn = 0;
    uint32_t cur, offset;
    char *buf = NULL;
    table = malloc(sizeof(mapping_table));
    if (!table) { 
        return NULL; 
    }
    // Create a dummy / starter table
    if (fd == DUMMY_TABLE) {
        table->entries = malloc(table_size * sizeof(table_entry *));
        if (!table->entries) { 
            return NULL; 
        }
        for (uint32_t i = 0; i < table_size; ++i) {
            table->entries[i] = malloc(sizeof(table_entry));
            table->entries[i]->phys_block = i;
            table->entries[i]->erase_count = 0;
            table->entries[i]->valid = true;
            table->entries[i]->dirty = false;
        }
        table->size = table_size;
    // Read table from flash memory
    } else {
        cur = 0;
        pages = table_size / size;
        if (table_size % size > 0) {
            ++pages;
        }
        table->entries = malloc(table_size * sizeof(table_entry *));
        table->size = table_size;
        for (uint32_t i = 0; i < pages; ++i, ++ppn) {
            if (ppn > PAGES_PER_BLOCK) {
                fd = get_next_block(++fd);
                ppn = 0;
            }
            page_read(fd, ppn, (void **)&buf);
            offset = 0;
            if (i == pages - 1) {
                size = table_size % size;
            }
            for (uint32_t j = 0; j < size; ++j, offset+=ENTRY_SIZE) {
                table->entries[cur++] = deserialize_te(buf+offset);
            }
            if (buf != NULL) {
                free(buf);
                buf = NULL;
            }
        }
    }
    free(buf);
    return table;
}

int32_t flush_table(mapping_table *table, uint32_t eraseblock, uint32_t ppn) 
{
    /*
     * table: mapping table held in memory
     * ppn: physical page number to start recording table
     * return: ppn where table was flushed
     */

    if (!table) { 
        return false; 
    }

    uint32_t size = ENTRIES_PER_PAGE;
    uint32_t pages = table->size / size;
    if (table->size % size > 0) {
        ++pages;
    }
    uint32_t offset = 0;
    char *buffer;
    if (pages == 0) {
        pages = 1;
    }

    eraseblock = erase_ftl_block(eraseblock);
    for(uint32_t i = 0; i < pages; ++i) {
        buffer = serialize_te_page(table, offset);
        offset+=ENTRIES_PER_PAGE;
        page_write(eraseblock, ppn++, buffer, PAGESIZE);
        if (ppn > PAGES_PER_BLOCK) {
            eraseblock = get_next_block(++eraseblock);
            eraseblock = erase_ftl_block(eraseblock);
            ppn = 0;
        }
        if (buffer) {
            free(buffer);
        }
    }

    return true;
}

boolean free_table(mapping_table *table) 
{
    /*
     * table: the mapping table
     * deallocate memory for the table
     * return: boolean
     */
    if (!table) { 
        return false; 
    }
    for (uint32_t i = 0; i < table->size; ++i) {
        free(table->entries[i]);
    }
    free(table->entries);
    free(table);
    return true;
}

boolean swap_table_entry(mapping_table *table, uint32_t i, uint32_t j) 
{
    if (!table) { 
        return false; 
    }
    if (i < MTABLE_BLOCKS ) { 
        return false; 
    }

    table_entry *temp = table->entries[i];
    table->entries[i] = table->entries[j];
    table->entries[j] = temp;

    return true;
}

char *serialize_te(table_entry *entry) 
{
    if (!entry) { 
        return NULL; 
    }

    char *buf = malloc(ENTRY_SIZE);
    uint32_t off = 0;

    memcpy(buf, &entry->phys_block, PHYS_BLOCK_SIZE);
    off += PHYS_BLOCK_SIZE;
    memcpy(buf + off, &entry->erase_count, ERASE_COUNT_SIZE);
    off += ERASE_COUNT_SIZE;
    memcpy(buf + off, &entry->valid, VALID_SIZE);
    off += VALID_SIZE;
    memcpy(buf + off, &entry->dirty, VALID_SIZE);
    return buf;
}

table_entry *deserialize_te(void *serialized_entry) 
{
    if (!serialized_entry) { 
        return NULL; 
    }

    uint32_t off = 0;

    table_entry *entry = malloc(sizeof(table_entry));
    memcpy(&entry->phys_block, serialized_entry, PHYS_BLOCK_SIZE);
    off += PHYS_BLOCK_SIZE;
    memcpy(&entry->erase_count, serialized_entry + off, ERASE_COUNT_SIZE);
    off += ERASE_COUNT_SIZE;
    memcpy(&entry->valid, serialized_entry + off, VALID_SIZE);
    off += VALID_SIZE;
    memcpy(&entry->dirty, serialized_entry + off, VALID_SIZE);
    return entry;
}

char *serialize_te_page(mapping_table *table, uint32_t offset) 
{
    if (!table) { 
        return NULL; 
    }

    uint32_t end = offset + ENTRIES_PER_PAGE;
    uint32_t bufsize, off;
    if (end > table->size) {
        end = table->size;
    }

    // check bound to see if there's anything to write
    off = 0;
    uint32_t size = ENTRY_SIZE;
    bufsize = size * (end - offset);
    if (bufsize < 1) { 
        return NULL; 
    }

    // allocate buffers
    char *serialized_entry = NULL;
    char *buf = malloc(bufsize);
    memset(buf, 0, bufsize);

    // fill page buffer with serialized table entries
    for (uint32_t i = offset; i < end; ++i) {
        serialized_entry = serialize_te(table->entries[i]);
        if (serialized_entry != NULL) {
            memcpy(buf+off, serialized_entry, ENTRY_SIZE);
            off += ENTRY_SIZE;
            free(serialized_entry);
        }
    }
    return buf;
}

boolean set_valid_bit(mapping_table *table, uint32_t lpn, boolean valid) {
    if (!table) { 
        return false; 
    }

    uint32_t logical_block = lpn / PAGES_PER_BLOCK;
    table->entries[logical_block]->valid = valid;
    return true;
}

boolean set_dirty_bit(mapping_table *table, uint32_t lpn, boolean dirty) {
    if (!table) { return false; }

    uint32_t logical_block = lpn / PAGES_PER_BLOCK;
    table->entries[logical_block]->dirty = dirty;
    return true;
}
