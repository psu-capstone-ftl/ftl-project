#ifndef GC_H
#define GC_H

#include "mapping_table.h"
#include "nand.h"
#include "common.h"

// Main garbage collection function
// Return false on failure, true on success
boolean garbage_collect(mapping_table *table);

// Helper function to swap the victim block and the new block
// Return false on failure, true on success
boolean garbage_collect_block(mapping_table *table, table_entry *victim);

// Helper function to select the block to be garbage collected
// returns a block
table_entry *select_victim_block(mapping_table *table);

// Helper function to find a free block from the gc reserverd area
table_entry *get_next_free_block(mapping_table *table);

#endif

