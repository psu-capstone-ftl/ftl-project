#ifndef COMMON_H
#define COMMON_H

#include "mapping_table.h"
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>

#define BLOCKSIZE (PAGES_PER_BLOCK * PAGESIZE)
#define DIRTY_BYTE 9
#define INVALID_BLOCK 0xffffffff

boolean FTL_INIT;

void *BB_TABLE, *M_TABLE;

typedef uint32_t BLOCK;

typedef uint32_t PAGE;

// Temporary measure to allow compile
// TODO: Need to decide the structure of the spare area, where to flag,
// look for erase marker, bb marker, etc
u_int64_t ERASE_MASK;

// All of the error codes should be less than 0
typedef enum{
    OUT_OF_RESERVED_BLOCKS = -1,
    READ_FAILED = -2,
    WRITE_FAILED = -3,
    BAD_ALIGNMENT = -4,
    NULL_BUFFER = -5,
    DEV_OPEN_FAIL = -6,
    READ_WRITE_BEYOND_DEV_SIZE = -7,
    NO_FTL_INIT = -8,
    HARDWARE_ISSUE = -9,
    INVALID_ADDRESS = -10,
    OOB_ERROR = -11
} etype;

typedef enum{
    READ = 1,
    WRITE = 2
}iotype;

#endif
