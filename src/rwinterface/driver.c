#define _XOPEN_SOURCE 500
#define _BSD_SOURCE
#define _POSIX_C_SOURCE 200809L

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/ioctl.h>
#include "common.h"
#include "libmtd.h"
#include "../nand.h"
#include "mtd/mtd-user.h"

/* Much of this code is from nandwrite and naddump which are
 *  part of the mtd-utils library and this code is thus GPL 2
 * 
 *  nanddump copyright stuff
 *  Copyright (C) 2000 David Woodhouse (dwmw2@infradead.org)
 *                     Steven J. Hill (sjhill@realitydiluted.com)
 *
 *
 * nandwrite copyright stuff:
 *  Copyright (C) 2000 David Woodhouse (dwmw2@infradead.org)
 *                     Steven J. Hill (sjhill@realitydiluted.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
uint32_t page_write(uint32_t eraseblock, uint32_t page, void * input, size_t inputlen)
{
    int fd = -1;
    int pagelen;
    struct mtd_dev_info mtd;
    bool failed = true;
    libmtd_t mtd_desc = 0;
    int ebsize_aligned;
    uint8_t write_mode;
    unsigned char * goalbuf = 0;
    static int blockalign = 1;
    etype error_type;

    static const char *mtd_device = "/dev/mtd0";

    if(!input) {
        error_type = NULL_BUFFER;
        goto closeall;
    }

    if ((fd = open(mtd_device, O_RDWR)) == -1) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    mtd_desc = libmtd_open();
    if (!mtd_desc) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if (mtd_get_dev_info(mtd_desc, mtd_device, &mtd) < 0) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if(eraseblock < 0 || eraseblock >= mtd.eb_cnt) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    if(page < 0 || page >= (mtd.eb_size/mtd.min_io_size)) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }

    ebsize_aligned = mtd.eb_size * blockalign;
    
    pagelen = mtd.min_io_size;

    long long mtdoffset = page * pagelen;

    write_mode = MTD_OPS_PLACE_OOB;

    /* Check, if length fits into device */
    if ((inputlen / pagelen) * pagelen > mtd.size - mtdoffset) {
        error_type = BAD_ALIGNMENT;
        goto closeall;
    }

    long long goalsize = ebsize_aligned / pagelen * pagelen;

    while(inputlen > goalsize) {
        goalsize += mtd.min_io_size;
    }

    goalbuf = xmalloc(goalsize);
    char * c_input = (char *)input;

    int i = 0;
    for(i = 0; i < inputlen; ++i) {
        goalbuf[i] = c_input[i];
    }
    for(; i < goalsize; ++i) {
        goalbuf[i] = 0xFF;
    }

    mtdoffset += goalsize * eraseblock;

    if(mtd_write(mtd_desc, &mtd, fd, mtdoffset / mtd.eb_size,
     mtdoffset % mtd.eb_size,
     goalbuf,
     mtd.min_io_size,
     NULL,
     0,
     write_mode) < 0) {
        error_type = HARDWARE_ISSUE;
        goto closeall;
    }

    failed = false;

closeall:
    if(goalbuf) {
        free(goalbuf);
    }
    if(mtd_desc) {
        libmtd_close(mtd_desc);
    }
    if(fd) {
        close(fd);
    }

    if(failed) {
        return (uint32_t)error_type;
    }
    /* Return happy */
    return mtd.min_io_size;
}

uint32_t page_read(uint32_t eraseblock, uint32_t page, void ** buf)
{
    long long ofs = 0;
    int ofd = 0, bs;
    int fd = 0;
    struct mtd_dev_info mtd;
    libmtd_t mtd_desc = 0;
    etype error_type;

    /*if(!buf) {
        error_type = NULL_BUFFER;
        goto closeall;
    }*/

    mtd_desc = libmtd_open();
    if (!mtd_desc) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    char * mtddev = "/dev/mtd0";

    if ((fd = open(mtddev, O_RDONLY)) == -1) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if (mtd_get_dev_info(mtd_desc, mtddev, &mtd) < 0) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if(eraseblock < 0 || eraseblock >= mtd.eb_cnt) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    if(page < 0 || page >= (mtd.eb_size/mtd.min_io_size)) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }

    *buf = xmalloc(mtd.min_io_size);

    long pagelen = mtd.min_io_size;
    static int blockalign = 1;
    long long ebsize_aligned = mtd.eb_size * blockalign;
    long long goalsize = ebsize_aligned / pagelen * pagelen;
    ofs += eraseblock * goalsize;
    ofs += page * pagelen;

    if (ioctl(fd, MTDFILEMODE, MTD_FILE_MODE_RAW) != 0) {
            error_type = READ_FAILED;
            goto closeall;
    }

    if (ofs & (mtd.min_io_size - 1)) {
        error_type = BAD_ALIGNMENT;
        goto closeall;
    }

    if (ofs & (mtd.min_io_size - 1)) {
        error_type = BAD_ALIGNMENT;
        goto closeall;
    }

    bs = pagelen;

    if(mtd_read(&mtd, fd, eraseblock, page * mtd.min_io_size, *buf, bs) < 0) {
        error_type = HARDWARE_ISSUE;
        goto closeall;
    }

    close(fd);
    close(ofd);
    libmtd_close(mtd_desc);

    /* Exit happy */
    return mtd.min_io_size;

closeall:
    close(fd);
    close(ofd);
    if(mtd_desc) {
        libmtd_close(mtd_desc);
    }
    return (uint32_t)error_type;

}

uint32_t oob_read(uint32_t eraseblock, uint32_t page, void ** buf)
{
    long long ofs = 0;
    int ofd = 0;
    int fd = 0;
    struct mtd_dev_info mtd;
    libmtd_t mtd_desc = 0;
    etype error_type;
/*
	if(!buf) {
		error_type = NULL_BUFFER;
		goto closeall;
	}
*/
    mtd_desc = libmtd_open();
    if (!mtd_desc) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    char * mtddev = "/dev/mtd0";

    if ((fd = open(mtddev, O_RDONLY)) == -1) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if (mtd_get_dev_info(mtd_desc, mtddev, &mtd) < 0) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if(eraseblock < 0 || eraseblock >= mtd.eb_cnt) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    if(page < 0 || page >= (mtd.eb_size/mtd.min_io_size)) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    } 

    //*buf = xmalloc(mtd.oob_size);
    *buf = calloc(mtd.oob_size, 1);

    long pagelen = mtd.min_io_size;
    static int blockalign = 1;
    long long ebsize_aligned = mtd.eb_size * blockalign;
    ofs += eraseblock * ebsize_aligned;
    ofs += page * pagelen;

    if (ioctl(fd, MTDFILEMODE, MTD_FILE_MODE_RAW) != 0) {
            error_type = READ_FAILED;
            goto closeall;
    }

    if (ofs & (mtd.min_io_size - 1)) {
        error_type = BAD_ALIGNMENT;
        goto closeall;
    }

    if (ofs & (mtd.min_io_size - 1)) {
        error_type = BAD_ALIGNMENT;
        goto closeall;
    }

    if(mtd_read_oob(mtd_desc, &mtd, fd, ofs, mtd.oob_size - 1, *buf) < 0) {
        error_type = HARDWARE_ISSUE;
        goto closeall;
    }

    close(fd);
    close(ofd);
	libmtd_close(mtd_desc);

    /* Exit happy */
    return (uint32_t)mtd.oob_size;

closeall:
    close(fd);
    close(ofd);
	if(mtd_desc) {
		libmtd_close(mtd_desc);
	}

    return (uint32_t)error_type;
}

uint32_t oob_write(uint32_t eraseblock, uint32_t page, void * buf, size_t inputlen)
{
    int fd = -1;
    int pagelen;
    char * goalbuf = 0;
    struct mtd_dev_info mtd;
    bool failed = true;
    libmtd_t mtd_desc = 0;
    uint8_t write_mode;
    etype error_type;
    
    static const char *mtd_device = "/dev/mtd0";

    if(!buf) {
        error_type = NULL_BUFFER;
        goto closeall;
    }

    if ((fd = open(mtd_device, O_RDWR)) == -1) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    mtd_desc = libmtd_open();
    if (!mtd_desc) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if (mtd_get_dev_info(mtd_desc, mtd_device, &mtd) < 0) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    pagelen = mtd.min_io_size;

    if(inputlen > mtd.oob_size || inputlen < 0) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    if(eraseblock < 0 || eraseblock >= mtd.eb_cnt) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    if(page < 0 || page >= (mtd.eb_size/pagelen)) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    
    pagelen = mtd.min_io_size;

    long long mtdoffset = page * pagelen;

    if (mtdoffset & (mtd.min_io_size - 1)) {
        error_type = BAD_ALIGNMENT;
        goto closeall;
    }
    write_mode = MTD_OPS_PLACE_OOB;

    /* Check, if length fits into device */
    if ((inputlen / pagelen) * pagelen > mtd.size - mtdoffset) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }
    
    long long goalsize = mtd.oob_size;
    goalbuf = xmalloc(goalsize);
    char * c_input = (char *)buf;

    int i = 0;
    for(i = 0; i < inputlen; ++i) {
        goalbuf[i] = c_input[i];
    }
    for(; i < goalsize; ++i) {
        goalbuf[i] = 0xFF;
    }

    mtdoffset += mtd.eb_size * eraseblock;

    if(mtd_write(mtd_desc, &mtd, fd, mtdoffset / mtd.eb_size,
     mtdoffset % mtd.eb_size,
     NULL,
     0,
     goalbuf,
     inputlen,
     write_mode) < 0) {
        error_type = HARDWARE_ISSUE;
        goto closeall;
    }

    failed = false;

closeall:
    if(goalbuf) {
        free(goalbuf);
    }
    if(mtd_desc) {
        libmtd_close(mtd_desc);
    }
    if(fd) {
        close(fd);
    }

    if(failed == true) {
        return error_type;
    }

    /* Return happy */
    return mtd.oob_size;
}

uint32_t _erase(uint32_t eraseblock)
{
    int fd = -1;
    struct mtd_dev_info mtd;
    bool failed = true;
    /* contains all the data read from the file so far for the current eraseblock */
    libmtd_t mtd_desc = 0;
    etype error_type;

    static const char *mtd_device = "/dev/mtd0";

    if ((fd = open(mtd_device, O_RDWR)) == -1) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    mtd_desc = libmtd_open();
    if (!mtd_desc) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if (mtd_get_dev_info(mtd_desc, mtd_device, &mtd) < 0) {
        error_type = DEV_OPEN_FAIL;
        goto closeall;
    }

    if(eraseblock >= mtd.eb_cnt || eraseblock < 0) {
        error_type = READ_WRITE_BEYOND_DEV_SIZE;
        goto closeall;
    }

    if(mtd_erase(mtd_desc, &mtd, fd, eraseblock) < 0) {
        error_type = HARDWARE_ISSUE;
        goto closeall;
    }

    failed = false;

closeall:
    if(mtd_desc) {
        libmtd_close(mtd_desc);
    }
    if(fd) {
        close(fd);
    }

    if(failed == true) {
        return error_type;
    }
    return 0;

}
