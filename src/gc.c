#include "gc.h"

boolean garbage_collect(mapping_table *table)
{
    table_entry *victim = NULL;
    // If unitialized table or no entries
    if (!table || !table->size){
        return false;
    }

    // Select the block to be garbage collected
    victim = select_victim_block(table);

    // Deal with erasing the victim and swapping with the new block
    if(victim && garbage_collect_block(table, victim) == true){
        return true;
    }
    return false;
}

table_entry *select_victim_block(mapping_table *table)
{
    if (!table) {
        // Return Null if we can't find a victim
        return NULL;
    }
    table_entry *victim = NULL;
    table_entry *curr;
    int32_t lowest_erases = -1;

    // Loop through data blocks and find block with fewest erases
    for (uint32_t i = FIRST_DATA_BLOCK; i < table->size; ++i) {
        curr = table -> entries[i];
        if (curr->valid < 0) {
            // Handle finding the first invalid block
            if (lowest_erases == -1) {
                lowest_erases = curr->erase_count;
                victim = curr;
            } else if (curr->erase_count < lowest_erases) {
                lowest_erases = curr->erase_count;
                victim = curr;
            }

        }
    }

    // Will be NULL if no block is invalid
    return victim;

}

boolean garbage_collect_block(mapping_table *table, table_entry* victim)
{
    if (!victim || !table) {
        return false;
    }

    BLOCK victim_block = victim->phys_block;
    table_entry *free_entry = get_next_free_block(table);
    BLOCK free_block = free_entry->phys_block;
    // Erase victim block
    _erase(victim_block);
    victim->erase_count += 1;
    // Block is valid now that we have erased it
    victim->valid = true;
    // Unset dirty flag
    victim->dirty = false;
    // Swap table entries
    swap_table_entry(table, victim_block, free_block);
    return true;
}

table_entry *get_next_free_block(mapping_table *table)
{
    // Start at the first block in the GC reserved area
    int32_t gc_blocks_start = MTABLE_BLOCKS + BBM_BLOCKS;
    table_entry *new_block = table -> entries[gc_blocks_start];
    table_entry *curr = NULL;
    int32_t fewest_erases = table->entries[gc_blocks_start]->erase_count;

    // Loop through the gc reserved area to grab a new block
    for (int32_t i = gc_blocks_start; i < gc_blocks_start + GC_BLOCKS; ++i) {
        curr = table->entries[i];
        if (curr->erase_count < fewest_erases) {
            new_block = curr;
            fewest_erases = curr->erase_count;
        }
    }
    return new_block;
}
