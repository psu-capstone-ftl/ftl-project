/*
 *  Copyright (C) 2011 ISEE 2007, SL
 *  Author: Javier Martinez Canillas <martinez.javier@gmail.com>
 *  Author: Agusti Fontquerni Gorchs <afontquerni@iseebcn.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
*/

#ifndef ERROR_CORRECTION_H
#define ERROR_CORRECTION_H

#include "nand.h"
#include "common.h"

// Byte sizes and addresses
#define SECTORSIZE 512
#define OOB_SECTORS 4
#define SECTOR_ECCADDR 8
#define SPARE_ECCADDR 0xe
#define SECTOR_ECCLEN 3
#define SPARE_ECCLEN 2

#define ECC_MATCH 0
#define ECC_CORRECTION_MADE 1
#define TWO_BIT_CORRUPTION -1
#define SECT_CORRECTABLE 0xfff
#define SECT_EVEN_MASK 0xfff
#define SECT_ODD_MASK 0xfff000
#define SPARE_CORRECTABLE 0x1f
#define SPARE_EVEN_MASK 0x1f
#define SPARE_ODD_MASK 0x3e0
#define BIT_HALF_MASK 0x7
#define BYTE_HALF_POS 3

#define EVEN_WHOLE  0xff
#define EVEN_HALF   0x0f
#define ODD_HALF    0xf0
#define EVEN_FOURTH 0x33
#define ODD_FOURTH  0xcc
#define EVEN_EIGHTH 0x55
#define ODD_EIGHTH  0xaa

#define _L1(n)  (((n) < 2)     ?      0 :  1)
#define _L2(n)  (((n) < 1<<2)  ? _L1(n) :  2 + _L1((n)>>2))
#define _L4(n)  (((n) < 1<<4)  ? _L2(n) :  4 + _L2((n)>>4))
#define _L8(n)  (((n) < 1<<8)  ? _L4(n) :  8 + _L4((n)>>8))
#define LOG2(n) (((n) < 1<<16) ? _L8(n) : 16 + _L8((n)>>16))

// Called during write/read of page respectively
int32_t write_page_ecc(uint32_t block_num, uint32_t page_num, uint8_t* page);
/*
Before writing to a page, pass it through this function to generate an error
correction code that will be written to the out of bounds area. All that is
needed is the data to be written, and its location.
*/
int32_t read_page_ecc(void** buf, uint32_t block_num, uint32_t page_num);
/*
Before reading a page, pass it through this function to ensure that any
single-bit errors will be corrected before reading.
*/

#endif
