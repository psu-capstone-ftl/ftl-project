CC = gcc
CC_OPTS = -m32 -std=c99 -Wall $(DEFINES)
SRC_FILES := $(wildcard src/*.c) $(wildcard src/*.h) $(wildcard src/rwinterface/*.c)
SYSTEM_TEST_FILES = src/*.h src/rwinterface/*.c src/ftl_public.c src/bad_block.c src/error_correction.c src/mapping_table.c src/gc.c
DIST_OUT = dist/ftl
TEST_OPTS = -g -m32 -std=c99 -L/usr/lib $(DEFINES)
CMOCKA = -lcmocka
DEFINES = -DPAGES_PER_BLOCK=$(PAGES_PER_BLOCK) -DPAGESIZE=$(PAGESIZE) -DOOB_SIZE=$(OOB_SIZE) -DNUM_OF_BLOCKS=$(NUM_OF_BLOCKS) -DMTABLE_ADDR=$(MTABLE_ADDR)

NUM_OF_BLOCKS = 8192
PAGES_PER_BLOCK = 32
PAGESIZE = 512
OOB_SIZE = 16
MTABLE_ADDR = -1


build: clean
	$(CC) $(CC_OPTS) $(SRC_FILES) -s -o $(DIST_OUT)

debug: clean
	$(CC) $(CC_OPTS) $(SRC_FILES) -g -o $(DIST_OUT)

clean:
	find dist/ -type f -not -name '.gitignore' -delete

test: test_all

test_local:
	$(CC) $(TEST_OPTS) test/test_bad_block.c -o test/bad_block_tests $(CMOCKA)
	$(CC) $(TEST_OPTS) test/test_mapping_table.c -o test/mapping_table_tests $(CMOCKA)
	$(CC) $(TEST_OPTS) test/test_gc.c -o test/gc_tests $(CMOCKA)
	$(CC) $(TEST_OPTS) test/test_error_correction.c -o test/ecc_tests $(CMOCKA)
	test/bad_block_tests
	test/mapping_table_tests
	test/gc_tests
	test/ecc_tests

test_all: test_local
	$(CC) $(TEST_OPTS) test/test_ftl_public.c $(SYSTEM_TEST_FILES) -o test/ftl_public_tests $(CMOCKA)
	$(CC) $(TEST_OPTS) test/test_driver.c src/rwinterface/*.c -o test/driver_tests $(CMOCKA)
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/ftl_public_tests
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/driver_tests
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1

system_tests:
	$(CC) $(TEST_OPTS) $(SYSTEM_TEST_FILES) test/system.c -o test/system_tests $(CMOCKA)
	$(CC) $(TEST_OPTS) $(SYSTEM_TEST_FILES) test/sequential_write_test.c -o test/sequential_write $(CMOCKA)
	$(CC) $(TEST_OPTS) $(SYSTEM_TEST_FILES) test/repeated_write_test.c -o test/repeated_write $(CMOCKA)
	$(CC) $(TEST_OPTS) $(SYSTEM_TEST_FILES) test/repeated_write_and_then_read_test.c -o test/repeated_write_and_read $(CMOCKA)
	$(CC) $(TEST_OPTS) $(SYSTEM_TEST_FILES) test/random_write_test.c -o test/random_write_test $(CMOCKA)
	$(CC) $(TEST_OPTS) $(SYSTEM_TEST_FILES) test/test_system_gc.c -o test/system_gc_test $(CMOCKA)
	sudo test/system_tests
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/sequential_write
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/repeated_write
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/repeated_write_and_read
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/random_write_test
	sudo flash_eraseall /dev/mtd0 > /dev/null 2>&1
	sudo test/system_gc_test

.PHONY: test
