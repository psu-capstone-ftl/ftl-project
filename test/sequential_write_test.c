#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../src/ftl_public.h"

static void test_sequential_write(void ** state)
{
	int i = 0;
	char data = 'a';

	for(i = 0; i < (PAGESIZE * NUM_OF_BLOCKS)-PAGESIZE ; i+=PAGESIZE) {
		assert_int_equal(ftl_write(i, (void *)&data, 1), 1);
	}

}

int main(void)
{
    //init the shared structures
    ftl_init();
    const UnitTest tests[] =
    {
        unit_test(test_sequential_write),
    };
    return run_tests(tests);
}

