#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../src/error_correction.h"
#include "../src/error_correction.c"


#define TEST_BYTE 0
#define TEST_BIT 0


//mock stump for oob_read
uint32_t oob_read(uint32_t eraseblock, uint32_t page, void ** buf)
{
    return (uint32_t)mock();
}

//mock stump for oob_write
uint32_t oob_write(uint32_t eraseblock, uint32_t page, void * buf, size_t inputlen)
{
    return (uint32_t)mock();
}

//mock stump for page_read
uint32_t page_read(uint32_t eraseblock, uint32_t page, void ** buf)
{
    return (uint32_t)mock();
}



// Check calc bitwise parity
static void test_calc_bitwise_parity_even(void **state)
{
    assert_int_equal(calc_bitwise_parity(0xff, EVEN_HALF), 0);
}

static void test_calc_bitwise_parity_odd(void **state)
{
    assert_int_equal(calc_bitwise_parity(0xef, ODD_HALF), 1);
}

static void test_calc_bitwise_parity_whole(void **state)
{
    assert_int_equal(calc_bitwise_parity(0xef, EVEN_WHOLE), 1);
}


// Check calc byte row parity
static void test_calc_evenrow_parity_even(void **state)
{
    uint8_t byte_parities[SECTORSIZE];

    for (uint32_t i = 0; i < SECTORSIZE; ++i)
        byte_parities[i] = 0xff;

    assert_int_equal(calc_row_parity_bits(byte_parities, 1, 1, SECTORSIZE), 0);
}

static void test_calc_oddrow_parity_even(void **state)
{
    uint8_t byte_parities[SECTORSIZE];

    for (uint32_t i = 0; i < SECTORSIZE; ++i)
        byte_parities[i] = 0xff;

    assert_int_equal(calc_row_parity_bits(byte_parities, 0, 1, SECTORSIZE), 0);
}

static void test_calc_evenrow_parity_odd(void **state)
{
    uint8_t byte_parities[SECTORSIZE];

    for (uint32_t i = 0; i < SECTORSIZE; ++i)
        byte_parities[i] = 0xfe;
    byte_parities[SECTORSIZE-2] = 0x01;

    assert_int_equal(calc_row_parity_bits(byte_parities, 1, 1, SECTORSIZE), 1);
}

static void test_calc_oddrow_parity_odd(void **state)
{
    uint8_t byte_parities[SECTORSIZE];

    for (uint32_t i = 0; i < SECTORSIZE-1; ++i)
        byte_parities[i] = 0xfe;
    byte_parities[SECTORSIZE-1] = 0x01;

    assert_int_equal(calc_row_parity_bits(byte_parities, 0, 1, SECTORSIZE), 1);
}


// Calc sector ECC tests
static void test_calc_sector_ecc_even(void **state)
{
    uint8_t sector[SECTORSIZE];
    uint8_t code[SECTOR_ECCLEN];

    for (uint32_t i = 0; i < SECTORSIZE; ++i)
        sector[i] = 0xff;

    assert_int_equal(calc_sector_ecc(sector, code), 0);
}

static void test_calc_sector_ecc_odd(void **state)
{
    uint8_t sector[SECTORSIZE];
    uint8_t code[SECTOR_ECCLEN];

    for (uint32_t i = 0; i < SECTORSIZE; ++i)
        sector[i] = 0x00;
    sector[SECTORSIZE-1] = 0xfe;

    assert_int_not_equal(calc_sector_ecc(sector, code), 0);
}


// Bit correction test
static void test_perform_bit_correction(void** state)
{
    uint8_t sector[SECTORSIZE];
    uint8_t original_val;

    for (uint32_t i = 0; i < SECTORSIZE; ++i)
        sector[i] = 0xff;

    original_val = sector[TEST_BYTE];

    assert_int_not_equal(perform_bit_correction(sector, TEST_BYTE, TEST_BIT), 0);
    assert_int_not_equal(original_val, sector[TEST_BYTE]);
}


// Compare ECC tests
static void test_compare_ecc_good(void** state)
{
    uint8_t sector[SECTORSIZE];
    uint8_t old_ecc[SECTOR_ECCLEN];
    uint8_t new_ecc[SECTOR_ECCLEN];
    uint32_t i;

    for (i = 0; i < SECTORSIZE; ++i)
      sector[i] = 0xff;

    for(i = 0; i < SECTOR_ECCLEN; ++i){
        old_ecc[i] = 0xff;
        new_ecc[i] = 0xff;
    }

    assert_int_equal(compare_ecc(sector, old_ecc, new_ecc, 0), ECC_MATCH);
}


// Testing for situations where the error can be corrected
static void test_compare_ecc_correct(void** state)
{
    uint8_t sector[SECTORSIZE];
    uint8_t er_sector[SECTORSIZE];
    uint8_t old_ecc[SECTOR_ECCLEN];
    uint8_t new_ecc[SECTOR_ECCLEN];
    uint32_t i;

    for (i = 0; i < SECTORSIZE; ++i){
        sector[i] = 0x00;
        er_sector[i] = 0x00;
    }
    er_sector[0] = 0x01;

    calc_sector_ecc(sector, old_ecc);
    calc_sector_ecc(er_sector, new_ecc);

    assert_int_equal(compare_ecc(er_sector, old_ecc, new_ecc, 0), ECC_CORRECTION_MADE);
}

// Testing for situations where the error can't be corrected
static void test_compare_ecc_bad(void** state)
{
    uint8_t sector[SECTORSIZE];
    uint8_t old_ecc[SECTOR_ECCLEN];
    uint8_t new_ecc[SECTOR_ECCLEN];
    uint32_t i;
    uint8_t original_val;

    for (i = 0; i < SECTORSIZE; ++i)
        sector[i] = 0xff;

    for(i = 0; i < SECTOR_ECCLEN; ++i){
        old_ecc[i] = 0xff;
        new_ecc[i] = 0xfe;
    }

    original_val = sector[32];

    assert_int_equal(compare_ecc(sector, old_ecc, new_ecc, 0), TWO_BIT_CORRUPTION);
    assert_int_equal(original_val, sector[32]);
}


// Write ecc tests
static void test_write_ecc_good(void **state)
{
    uint8_t ecc[SECTOR_ECCLEN];
    uint8_t spare_ecc[SECTOR_ECCLEN];

    for (uint32_t i = 0; i < SECTOR_ECCLEN; ++i) {
        ecc[i] = 0x00;
        spare_ecc[i] = 0x00;
    }

    will_return(oob_write, OOB_SIZE);
    assert_int_equal(write_ecc(0, 0, 0, ecc, spare_ecc), OOB_SIZE);
}

static void test_write_ecc_bad(void **state)
{
    uint8_t* ecc = NULL, *spare_ecc = NULL;
    assert_int_equal(write_ecc(0, 0, 0, ecc, spare_ecc), -1);
}



// Calc spare ecc tests - redundant error correction
static void test_calc_spare_ecc_even(void** state)
{
    uint8_t spare[SECTOR_ECCLEN];
    uint8_t code[SPARE_ECCLEN];

    for (uint32_t i = 0; i < SECTOR_ECCLEN; ++i)
        spare[i] = 0x00;

    assert_int_equal(calc_spare_ecc(spare, code), 0x0);
}

static void test_calc_spare_ecc_odd(void** state)
{
    uint8_t spare[SECTOR_ECCLEN];
    uint8_t code[SPARE_ECCLEN];

    for (uint32_t i = 0; i < SECTOR_ECCLEN; ++i)
        spare[i] = 0x00;

    spare[SECTOR_ECCLEN-1] = 0xe0;

    assert_int_not_equal(calc_spare_ecc(spare, code), 0x0);
}

// Testing for situations where the error can be corrected
static void test_compare_ecc_correct_spare(void** state)
{
    uint8_t spare[SECTOR_ECCLEN];
    uint8_t er_spare[SECTOR_ECCLEN];
    uint8_t old_ecc[SPARE_ECCLEN];
    uint8_t new_ecc[SPARE_ECCLEN];
    uint32_t i;

    for (i = 0; i < SECTOR_ECCLEN; ++i) {
        spare[i] = 0x00;
        er_spare[i] = 0x00;
    }
    er_spare[SECTOR_ECCLEN-1] = 0x01;

    calc_spare_ecc(spare, old_ecc);
    calc_spare_ecc(er_spare, new_ecc);
    printf("old: %x  new: %x\n",*(uint16_t*)old_ecc, *(uint16_t*)new_ecc);

    assert_int_equal(compare_ecc(er_spare, old_ecc, new_ecc, 1), ECC_CORRECTION_MADE);
}

// Testing for situations where the error can't be corrected
static void test_compare_ecc_bad_spare(void** state)
{
    uint8_t spare[SECTOR_ECCLEN];
    uint8_t original_spare[SECTOR_ECCLEN];
    uint8_t old_ecc[SPARE_ECCLEN];
    uint8_t new_ecc[SPARE_ECCLEN];
    uint32_t i;

    for (i = 0; i < SECTOR_ECCLEN; ++i){
        spare[i] = 0xff;
        original_spare[i] = 0xff;
    }

    for(i = 0; i < SPARE_ECCLEN; ++i){
        old_ecc[i] = 0xff;
        new_ecc[i] = 0xfe;
    }

    assert_int_equal(compare_ecc(spare, old_ecc, new_ecc, 1), TWO_BIT_CORRUPTION);

    for (i = 0; i < SECTOR_ECCLEN; ++i)
        assert_int_equal(original_spare[i], spare[i]);
}



int main(void)
{
    const UnitTest tests[] =
    {
        unit_test(test_calc_bitwise_parity_even),
        unit_test(test_calc_bitwise_parity_odd),
        unit_test(test_calc_bitwise_parity_whole),
        unit_test(test_calc_evenrow_parity_even),
        unit_test(test_calc_oddrow_parity_even),
        unit_test(test_calc_evenrow_parity_odd),
        unit_test(test_calc_oddrow_parity_odd),
        //unit_test(test_calc_sector_ecc_even),
        unit_test(test_calc_sector_ecc_odd),
        unit_test(test_perform_bit_correction),
        unit_test(test_compare_ecc_good),
        unit_test(test_compare_ecc_correct),
        unit_test(test_compare_ecc_bad),
        unit_test(test_write_ecc_good),
        unit_test(test_write_ecc_bad),
        //unit_test(test_calc_spare_ecc_even),
        //unit_test(test_calc_spare_ecc_odd),
        //unit_test(test_compare_ecc_correct_spare),
        //unit_test(test_compare_ecc_bad_spare),
    };
    return run_tests(tests);
};
