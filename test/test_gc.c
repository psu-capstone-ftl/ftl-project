#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../src/gc.c"
#include "../src/mapping_table.h"
#include "../src/mapping_table.c"

// 'mock' the oob_read function
uint32_t oob_read(uint32_t eraseblock, uint32_t page, void ** buf){
    switch (page % 2) {
        case 0:
            ((char *)*buf)[4] = 1;
            break;
        case 1:
            ((char *)*buf)[4] = 0;
            break;
        default:
            ((char *)*buf)[4] = 0;
            break;
    }
    return 5;
}

// 'mock' the page_write function
uint32_t page_write(uint32_t eraseblock, uint32_t page, void * input, size_t inputlen){
    return 1;
}

// 'mock' the page_read function
uint32_t page_read(uint32_t eraseblock, uint32_t page, void ** buf){
    return 1;
}

//'mock' the _erase function
uint32_t _erase(uint32_t eraseblock){
    return 1;

}

static void test_select_victim(void **state)
{
    /*
     * WHAT: Tests that the correct victim is selected
     * HOW: Set all blocks to have the same erase count
     * WHY: Because we loop through the data blocks checking if the current block has fewer erases we should select the
     * the first data block
     * Note: FIRST_DATA_BLOCK is the index of the first non-reserved block
    */

    // Calling init with 0 Creates a dummy table for testing
    int32_t buf[PAGESIZE];
    mapping_table *table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    for(int32_t i = 0; i < table -> size; ++i){
        table -> entries[i]->erase_count = 1;
        table -> entries[i]->valid = false;
    }
    
    //First block is selected as all have the same erase count and invalid pages
    assert_int_equal(select_victim_block(table) -> phys_block, FIRST_DATA_BLOCK);
    free_table(table);
}

static void test_select_specific_victim(void **state)
{
    /*
     * WHAT: Tests that the correct victim is selected that is not the first data block
     * HOW: Set all blocks to have the same erase count, but set the expected victim block to have 1 less erase
     * WHY: Because we loop through the data blocks checking if the current block has fewer erases we should select the
     * the expected victim block because it has 1 less erase
     * Note: FIRST_DATA_BLOCK is the index of the first non-reserved block
    */
    uint32_t victim_block = FIRST_DATA_BLOCK + 3;
    mapping_table *table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    for(int32_t i = 0; i < table -> size; ++i){
        table -> entries[i]->erase_count = 1;
        table -> entries[i]->valid = false;
    }
    // We should always pick the block with the lowest number of erases.
    table -> entries[victim_block]->erase_count = 0;
    //First block is selected as all have the same erase count and invalid pages
    assert_int_equal(select_victim_block(table) -> phys_block, FIRST_DATA_BLOCK + 3);
    free_table(table);
}

static void test_select_victim_fail(void **state)
{
    /*
     * WHAT: Tests that victim selection fails if no blocks are marked invalid
     * HOW: Set all blocks to be invalid
     * WHY: Because we loop through the data blocks checking if the current block is invalid and has fewer erases
     * we shouldn't find a victim if all blocks are valid.
    */
    mapping_table *table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    for(int32_t i = 0; i < table -> size; ++i){
        table -> entries[i]->erase_count = 0;
        table -> entries[i]->valid = true;
    }
    // Because the table has no invalid blocks there should be no victim selected
    assert_int_equal(select_victim_block(table), NULL);
    free_table(table);
}

static void test_garbage_collection_success(void **state)
{
    /*
     * WHAT: Tests that garbage collection runs, selects the expected victim and actually swaps the physical blocks
     * HOW: Set all blocks to have the same erase count and call garbage collection
     * WHY: Because we loop through the data blocks checking if the current block has fewer erases we should select the
     * the first data block. We also can assume that selected free block is the first in the gc reserved area
     * Because garbage collection swaps the mapping table entries of the free block and the victim block we check
     * that the physical blocks have been swapped.
     * Note: FIRST_DATA_BLOCK is the index of the first non-reserved block
    */
    // Calling init with 0 Creates a dummy table for testing
    mapping_table *table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    for(int32_t i = 0; i < table -> size; ++i){
        table -> entries[i]->erase_count = 1;
        table -> entries[i]->valid = false;
    }
    assert_int_equal(garbage_collect(table), true);
    // We should have swapped the physical pages of the first block in the gc reserved
    // And the victim block, which in this case should be the FIRST_DATA_BLOCK
    assert_int_equal(table -> entries[FIRST_DATA_BLOCK]->phys_block, MTABLE_BLOCKS + BBM_BLOCKS);
    free_table(table);
}

static void test_garbage_collection_no_table(void **state)
{
    /*
     * WHAT: Tests that the garbage collection fails with the expected return value when no table is provided
     * HOW: Provide a null mapping table
     * WHY: The function should exit with 0 if the table is NULL
    */
    // Should fail if no table is defined
    mapping_table *table = NULL;
    assert_int_equal(garbage_collect(table), false);

    free_table(table);
}

static void test_garbage_collection_all_valid(void **state)
{
    /*
     * WHAT: Tests that garbage collection fails if there are no blocks that are marked invalid
     * HOW: Set all blocks to be valid
     * WHY: Because no blocks are invalid we can't find a victim to garbage collection
    */
    // Calling init with 0 Creates a dummy table for testing
    mapping_table *table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    for(int32_t i = 0; i < table -> size; ++i){
        table -> entries[i]->erase_count = 1;
        table -> entries[i]->valid = true;
    }
    // Should fail because we have no blocks to garbage collect
    assert_int_equal(garbage_collect(table), false);
    free_table(table);
}

static void test_gc_with_diff_free_block(void **state)
{
    /*
     * WHAT: Tests that we get can get a different free block and not just the first one
     * HOW: Set all blocks to have the same erase count, set the expected free block to have the lowest erase count
     * WHY: Because we take the free block with the fewest erases we should take the expected free block
     * Note: FIRST_DATA_BLOCK is the index of the first non-reserved block
    */
    uint32_t expected_free_block = MTABLE_BLOCKS + BBM_BLOCKS + 1;
    // Calling init with 0 Creates a dummy table for testing
    mapping_table *table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    for(int32_t i = 0; i < table -> size; ++i){
        table -> entries[i]->erase_count = 1;
        table -> entries[i]->valid = false;
    }
    table -> entries[expected_free_block]->erase_count =  0;
    assert_int_equal(garbage_collect(table), true);
    // We should have swapped the physical pages of the first block in the gc reserved
    // And the victim block, which in this case should be the FIRST_DATA_BLOCK
    assert_int_equal(table -> entries[FIRST_DATA_BLOCK]->phys_block, expected_free_block);
    free_table(table);
}

int main(void)
{
    const UnitTest tests[] =
    {
        unit_test(test_select_victim),
        unit_test(test_select_specific_victim),
        unit_test(test_select_victim_fail),
        unit_test(test_garbage_collection_success),
        unit_test(test_garbage_collection_no_table),
        unit_test(test_garbage_collection_all_valid),
        unit_test(test_gc_with_diff_free_block),
    };
    return run_tests(tests);
};
