#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <time.h>
#include "../src/nand.h"
#include "../src/bad_block.c"

#define BAD 0
#define GOOD 1


// Mock functions for testing
// Mock stump for oob_read
uint32_t oob_read(uint32_t eraseblock, uint32_t page, void** buf)
{
    uint16_t* data = malloc((OOB_SIZE/2)*sizeof(uint16_t));
    uint32_t i;

    switch(eraseblock){
      case(BAD):
        for(i = 0; i < OOB_SIZE/2; ++i)
          data[i] = ZERO;
        break;

      case(GOOD):
        for(i = 0; i < OOB_SIZE/2; ++i)
          data[i] = BBM_MARKER;
        break;

      default:
        for(i = 0; i < OOB_SIZE/2; ++i)
          data[i] = 0xaaaa;
        break;
    }

    *buf = (void*)data;

    return (uint32_t)mock();
}

// Mock stump for oob_write
uint32_t oob_write(uint32_t eraseblock, uint32_t page, void * buf, size_t inputlen)
{
    return (uint32_t)mock();
}

// Method to init structures for BBM for testing
int16_t* mock_bbm_init(boolean all_bad)
{
  nextResBlock = BBM_BLOCKS;
  srand(time(NULL));
  uint32_t block, r = 0;

  switch(all_bad) {
    case(true):
      for(block = 0; block < NUM_OF_BLOCKS; ++block)
        badBlockTable[block] = 0;
      break;

    case(false):
      for(block = 0; block < NUM_OF_BLOCKS; ++block)
        badBlockTable[block] = GOODBLOCK;
      break;

    default:
      for(block = 0; block < NUM_OF_BLOCKS; ++block){
        r = rand();
        if(r % 17 == 0)
          badBlockTable[block] = GOODBLOCK;
        else
          badBlockTable[block] = 0;
      }
      break;
  }

  return badBlockTable;
}




// Check block tests
static void test_checkBlock_good(void **state)
{
    uint16_t* bbm_bytes;
    uint16_t zero = ZERO;
    bbm_bytes = &zero;

    will_return(oob_read, OOB_SIZE);
    assert_int_equal(checkBlock(GOOD, bbm_bytes), 1);
}

static void test_checkBlock_bad(void **state)
{
    uint16_t* bbm_bytes;
    uint16_t zero = ZERO;
    bbm_bytes = &zero;

    will_return(oob_read, OOB_SIZE);
    assert_int_equal(checkBlock(BAD, bbm_bytes), 0);
}

static void test_checkBlock_error(void **state)
{
    uint16_t* bbm_bytes;
    uint16_t zero = ZERO;
    bbm_bytes = &zero;

    will_return(oob_read, READ_FAILED);
    assert_int_equal(checkBlock(BAD, bbm_bytes), READ_FAILED);
}

static void test_checkBlock_range(void **state)
{
    uint16_t* bbm_bytes;
    uint16_t zero = ZERO;
    bbm_bytes = &zero;
    assert_int_equal(checkBlock(NUM_OF_BLOCKS+1, bbm_bytes), -1);
}

// Next reserved block tests
static void test_nextReservedBlock_good(void** state)
{
    mock_bbm_init(false);
    assert_in_range(nextReservedBlock(), BBM_BLOCKS, FIRST_DATA_BLOCK);
}

// Case: run out of reserved space
static void test_nextReservedBlock_bad(void** state)
{
    mock_bbm_init(true);
    assert_not_in_range(nextReservedBlock(), BBM_BLOCKS, FIRST_DATA_BLOCK);
}

// Update bad block tests
static void test_updateBadBlocks_good(void** state)
{
  mock_bbm_init(false);
  assert_int_not_equal(updateBadBlocks(0), OUT_OF_RESERVED_BLOCKS);
}

// Case: run out of reserved space
static void test_updateBadBlocks_bad(void** state)
{
  mock_bbm_init(true);
  assert_int_equal(updateBadBlocks(0), OUT_OF_RESERVED_BLOCKS);
}

// Case: block number is invalid
static void test_updateBadBlocks_range(void** state)
{
  //mock_bbm_init(true);
  assert_int_equal(updateBadBlocks(NUM_OF_BLOCKS+1), -1);
}

// Write bad block tests - write successful or results in error
static void test_writeBlock_good(void** state)
{
  will_return(oob_write, BADBLOCK_LEN);
  assert_int_equal(writeBlock(0), BADBLOCK_LEN);
}

// Case: error while writing
static void test_writeBlock_bad(void** state)
{
  will_return(oob_write, WRITE_FAILED);
  assert_int_equal(writeBlock(0), WRITE_FAILED);
}

// Wrapper for bad block write tests
static void test_writeBadBlocks_good(void** state)
{
  will_return_always(oob_write, BADBLOCK_LEN);
  assert_in_range(writeBadBlocks(), 0, BADBLOCK_LEN);
}

// Case: error while writing
static void test_writeBadBlocks_bad(void** state)
{
  will_return(oob_write, WRITE_FAILED);
  assert_int_equal(writeBadBlocks(), WRITE_FAILED);
}


int main(void)
{
    const UnitTest tests[] =
    {
        unit_test(test_checkBlock_good),
        unit_test(test_checkBlock_bad),
        unit_test(test_checkBlock_error),
        unit_test(test_checkBlock_range),
        unit_test(test_nextReservedBlock_good),
        unit_test(test_nextReservedBlock_bad),
        unit_test(test_updateBadBlocks_good),
        unit_test(test_updateBadBlocks_bad),
        unit_test(test_updateBadBlocks_range),
        unit_test(test_writeBlock_good),
        unit_test(test_writeBlock_bad),
        unit_test(test_writeBadBlocks_good),
        unit_test(test_writeBadBlocks_bad),
    };
    return run_tests(tests);
};
