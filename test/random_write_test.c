#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../src/ftl_public.h"

/* This all needs to be completely rewritten. Simply here to give
   an idea of a test I'm thinking of trying. */
static void test_random_write(void ** state)
{
	int i = 0;
	char data = 'a';
	int rand_num = 0;
	int multiplier = (PAGESIZE * NUM_OF_BLOCKS) - PAGESIZE;

	srand(time(NULL));

	/*
	   The number 1070 is chosen because Linux gets mad
	   when the driver opens too many files
	*/

	for(i = 0; i < 1070; ++i) {
		rand_num = rand() % multiplier;
		if(rand_num == 0) {
			rand_num += 512;
		}
		assert_int_equal(ftl_write(rand_num / PAGESIZE, (void *)&data, 1), 1);
		assert_int_equal(ftl_erase(rand_num / PAGESIZE, 1), PAGESIZE);
	}
}

int main(void)
{
    //init the shared structures
    ftl_init();
    const UnitTest tests[] =
    {
        unit_test(test_random_write),
    };
    return run_tests(tests);
}

