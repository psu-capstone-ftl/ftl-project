#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../src/ftl_public.h"

static void test_repeated_then_read(void ** state)
{
	int i = 0;
	char data = 'a';
	char * ret = malloc(sizeof(char *));

	/*
	   The number 1070 was chosen because Linux gets
	   mad when you open too many files and shuts 
	   stuff down
	*/

	for(i = 0; i < 1070; ++i) { /* Number is arbitrary; doesn't crash driver */
		assert_int_equal(ftl_write(0, (void *)&data, 1), 1);
		assert_int_equal(ftl_erase(0, 1), PAGESIZE);
	}
		
	assert_int_equal(ftl_read(0, ret, 1), 1);
	free(ret);

}

int main(void)
{
    //init the shared structures
    ftl_init();
    const UnitTest tests[] =
    {
        unit_test(test_repeated_then_read),
    };
    return run_tests(tests);
}

