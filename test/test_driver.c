#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../src/nand.h"
#include "../src/common.h"

/* Tests page_write and its edge cases, seeing if it 
   throws errors where it makes sense. */
static void test_page_write(void ** state)
{
	
	char * value1 = "La dee la la";
	char * null_value = NULL;
	assert_int_equal(page_write(-2, 2, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(99999999, 2, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(1, -1, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(1, 9999999, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(1, 9999999, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(0, 32, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(8192, 0, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(page_write(1,2, (char *)null_value, 0), (uint32_t)NULL_BUFFER);
	assert_int_equal(page_write(8191, 0, (char *)value1, strlen(value1)), (uint32_t)512);
	assert_int_equal(page_write(0, 31, (char *)value1, strlen(value1)), (uint32_t)512);
	page_write(1,2, (char *)value1, strlen(value1));

}

/*This function tests the error codes and edge cases of
  page_read to see if they make sense */
static void test_page_read(void ** state)
{
	char* buf = NULL;
	char ** null_buf = NULL;

	assert_int_equal(page_read(-1, 1, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}

	assert_int_equal(page_read(1, -1, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}

	assert_int_equal(page_read(99999999, 1, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}

	assert_int_equal(page_read(1,99999999, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}

	assert_int_equal(page_read(8192, 0, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}

	assert_int_equal(page_read(8191, 0, (void **)&buf), (uint32_t)512);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}

	assert_int_equal(page_read(0, 32, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
    	if (buf) {
            free(buf);
            buf = NULL;
    	}
	//assert_int_equal(page_read(1, 2, (void **)null_buf), (uint32_t)NULL_BUFFER);
}

/* This function is meant to test the edge
   cases of test_oob_read to make sure reasonabl
   edge cases are returned. */
static void test_oob_read(void ** state)
{
        char * buf = NULL;
	char ** null_buf = NULL;

	assert_int_equal(oob_read(-1, 1, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(1, -1, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(9999999, 1, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(1, 9999999,(void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(1, 9999999,(void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(1, 9999999,(void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(8192, 0, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(0, 32, (void **)&buf), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(0, 31, (void **)&buf), (uint32_t)16);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	assert_int_equal(oob_read(8191, 0, (void **)&buf), (uint32_t)16);
        if(buf) {
            free(buf);
            buf = NULL;
        }  

	char * val = "012345678901234";
        char * ret = NULL;

	oob_write(4, 2, (char *)val, strlen(val));
	oob_read(4, 2, (void **)&ret);
	int result = -1;
	if(ret) {
	    result = strcmp(val, ret);
        } 
	assert_int_equal(0, result);
	free(ret);
}

/* This tests the edge cases of oob_write and 
   sees if reasonable error codes are returned. */
static void test_oob_write(void ** state)
{
	char * value1 = "La dee la la";
	char * null_value = NULL;

	assert_int_equal(oob_write(-2, 2, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(99999999, 2, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(1, -1, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(1, 9999999, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(9999999, 1, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(0, 32, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(8192, 0, (char *)value1, strlen(value1)), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(oob_write(1,2, (char *)null_value, 0), (uint32_t)NULL_BUFFER);
	assert_int_equal(oob_write(8191, 0, (char *)value1, strlen(value1)), (uint32_t)16);
	assert_int_equal(oob_write(0, 31, (char *)value1, strlen(value1)), (uint32_t)16);
	
}

/* This is a test of things to be checked via 
   nanddump. */
static void test_manual_write_check(void ** state)
{
	char * val1 = "A";
	char * val2 = "B";
	char * val3 = "C";
	char * val4 = "D";
	char * val5 = "E";
	char * val6 = "F";

	_erase(1);
	page_write(0,0, (char *)val1, strlen(val1));
	page_write(0, 1, (char *)val2, strlen(val2));
	page_write(0, 2, (char *)val3, strlen(val3));
	page_write(1, 0, (char*)val4, strlen(val4));
	page_write(2, 0, (char *)val5, strlen(val5));
	page_write(3, 0, (char *)val6, strlen(val6));
}

/* This is a test of the erase function to make sure the edge
   case respones make sense. */
static void test_erase(void ** state)
{
	assert_int_equal(_erase(-1), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(_erase(8192), (uint32_t)READ_WRITE_BEYOND_DEV_SIZE);
	assert_int_equal(_erase(0), (uint32_t)0);
	assert_int_equal(_erase(8191), (uint32_t)0);

}

int main(int argc, char ** argv)
{
	const UnitTest tests[] =
	{
		unit_test(test_page_write),
		unit_test(test_page_read),
		unit_test(test_oob_write),
		unit_test(test_oob_read),
		unit_test(test_manual_write_check),
		unit_test(test_erase),
	};

	return run_tests(tests);
}
